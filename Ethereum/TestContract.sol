// SPDX-License-Identifier: MIT

pragma solidity >= 0.8.0 <0.9.0;

contract Test {
    uint256 number;
    address myAddress;
    
    constructor() {
        myAddress = msg.sender;
    }
    
    function store(uint256 _number) public {
        number = _number;
    }
    
    function viewNumber() public view returns(uint256) {
        return number;
    }
    
    function viewAddress() view public returns(address) {
        return myAddress;
    }
}
