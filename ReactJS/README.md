# Training materials   
### The folder contains source code for ReactJS training.
* __ReactBasics.html__: Contains some examples showcasing React basics such as functions/class component, state/props.
* __TodoApp.html__: Contains a sample to-do app written in React where you can add, delete and mark as complete for to-do items.
* __TicTacToe.html__: Contains a tic-tac-toe game written in React.
* __Testing.html__: For testing javascript code only.
