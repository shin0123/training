import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity } from 'react-native';

export default function App() {

  const [name, setName] = useState('');
  const [age, setAge] = useState(0);

  const onPress = (e) => { alert('Button Presssed');}
  return (
    <View style={styles.container}>
      <Text>Hello World</Text>
      <Text>
        Text 1 
        <Text style={styles.text}>
          Text 2
          <Text>Text 3
            <Text>Text 4</Text>
          </Text>
        </Text>
      </Text>

      <Text>
        <Text>First part and </Text>
        <Text>second part</Text>
      </Text>

      <View>  
        <Text>First part and </Text>  
        <Text>second part</Text>
      </View>

      <View style={styles.container2}>
        <Text style={styles.title}>React Native</Text>
      </View>

      <View>
        <Text>Name:</Text>
        <TextInput style={styles.input} onChangeText={setName} placeholder="please key in your name"></TextInput>
        <Text>Input name is: {name}</Text>

        <Text>Age:</Text>
        <TextInput style={styles.input} onChangeText={setAge} placeholder="please key in your age" keyboardType="numeric"></TextInput>
        <Text>Input age is: {age}</Text>
      </View>

      <Button title="Click Me" onPress={onPress}/>

      <TouchableOpacity style={styles.button} onPress={onPress} >
          <Text>Press Here</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'red',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  textInput: {
    width: 200,
    borderWidth: 1,
    borderColor: 'black',
    margin: 10
  },
  container2: {
    padding: 24,
    backgroundColor: "#eaeaea",
    color: "#eaeaea"
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  input: {
    borderWidth: 1,
    width: 200
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  }
});
