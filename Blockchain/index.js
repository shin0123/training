const crypto = require("crypto");

class Transaction {
	constructor(amount, sender, receiver) {
		this.amount = amount;
		this.sender = sender;
		this.receiver = receiver;
	}

	toString() {
		return JSON.stringify(this);
	}
}

class Block {
	constructor(previousHash, transaction, signature, blockNumber, timestamp = Date.now()) {
		this.previousHash = previousHash;
		this.transaction = transaction;
		this.signature = signature;
		this.blockNumber = blockNumber;
		this.timestamp = timestamp;
		this.nounce = Math.round(Math.random() * 10000);
	}

	getHash() {
		const json = JSON.stringify(this);
		const hash = crypto.createHash("SHA256");
		hash.update(json);
		hash.end();
		const hex = hash.digest("hex");
		return hex;
	}

	mine() {
		console.log(`Initial nounce:${this.nounce}\n\u26CF mining...`);
		const t0 = performance.now();
		while (true) {
			if (this.getHash().substring(0, 3) == '000') {
				const t1 = performance.now();
				console.log(`\x1b[31m\u2714 Solution found:${this.nounce} in ${t1 - t0} milliseconds\x1b[89m\x1b[0m\x1b[0m`);
				break;
			}
			this.nounce++;
		}
	}

	toString() {
		return JSON.stringify(this);
	}
}

class Chain {
	constructor() {
		let initialBlock = new Block(null, new Transaction(100, 'genesis', 'genesis'), null, 1);
		initialBlock.mine();
		this.blocks = [initialBlock];
	}

	getPreviousBlockHash() {
		return this.blocks[this.blocks.length - 1].getHash();
	}

	insertBlock(transaction, senderPublicKey, sig) {
		const verify = crypto.createVerify("SHA256");
		verify.update(transaction.toString());
		const isValid = verify.verify(senderPublicKey, sig);
		if (isValid) {
			const block = new Block(this.getPreviousBlockHash(), transaction, sig, this.blocks.length + 1);
			block.mine();
			this.blocks.push(block);
			console.log("Block added");
		}
	}
}

class Wallet {
	#privateKey = null;
	constructor(name) {
		const keys = crypto.generateKeyPairSync("rsa", {
			modulusLength: 2048,
			publicKeyEncoding: { type: "spki", format: "pem" },
			privateKeyEncoding: { type: "pkcs8", format: "pem" },
		});
		this.#privateKey = keys.privateKey;
		this.publicKey = keys.publicKey;
		this.name = name;
		console.log(`Wallet created: ${name}\nPublic Key:\n${keys.publicKey}`);
	}

	send(amount, recieverPublicKey) {
		const transaction = new Transaction(
			amount,
			this.publicKey,
			recieverPublicKey
		);
		console.log(transaction);
		const shaSign = crypto.createSign("SHA256");
		shaSign.update(transaction.toString());
		shaSign.end();
		const signature = shaSign.sign(this.#privateKey);
		chain.insertBlock(transaction, this.publicKey, signature);
	}
}

let chain = new Chain();
let alice = new Wallet('alice');
let bob = new Wallet('bob');
let carol = new Wallet('carol');
alice.send(100, bob.publicKey);
bob.send(50, alice.publicKey);
carol.send(10, bob.publicKey);
console.log(chain);

