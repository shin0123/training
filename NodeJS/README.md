# Training materials   
### The folder contains source code for NodeJS training. Ths is a simple blog website built with MERN stack.
* __Server.js__: Contains a sample NodeJS server.
* __ServerExpress.js__: Contains a sample NodeJS server built with express framework.
* __Index.html__: Contains home page for the website.
* __About.html__: Contains about me page for the website.
* __404.html__: Contains 404 error page for the website.
* __package.json__: Config file for NPM.
* __.env__: Contains environment variables for NodeJS.

* __assets__: Contains static files.
* __components__: Contains React component for rendering web pages.
* __models__: Contains Mongoose ORM models for mapping MongoDB document to javascript objects.
* __styles__: Contains CSS files.


