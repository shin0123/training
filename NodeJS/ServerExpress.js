require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const Blog = require('./models/blog.js')
const app = express();

mongoose.pluralize(null);

app.use(express.static('assets'));
app.use(express.static('components'));
app.use(express.static('styles'));

//to support URL encoded or JSON bodies
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use((req, res, next) => {
//     console.log(req.url);
//     next();
// });
app.get('/', (req, res) => res.sendFile('./Index.html', {root: __dirname}));
app.get('/about', (req, res) => res.sendFile('./About.html', {root: __dirname}));
app.get('/aboutme', (req, res) => res.redirect('/about'));
app.get('/blog', (req, res) => Blog.find().sort({createdAt: -1}).then((result) => res.send(result)));
app.post('/blog', (req, res) => {
    const blog = new Blog({
        title: req.body.title,
        content: req.body.content
    });
    blog.save()
        .then((result) => {
            console.log(`\x1b[32mBlog ${result._id} created sucessfully\x1b[89m\x1b[0m\x1b[0m`);
            res.redirect('/');
        })
        .catch((err) => console.log(err));
});
app.post('/blog/:id', (req, res) => {
    const id = req.params.id;
    Blog.findByIdAndUpdate(id, {
        title: req.body.title,
        content: req.body.content})
        .then(() =>{
            console.log(`\x1b[33mBlog ${id} updated successfully\x1b[89m\x1b[0m\x1b[0m`);
            res.redirect('/');
        })
        .catch((err) => console.log(err));
})
app.delete('/blog/:id', (req, res) => {
    const id = req.params.id;
    Blog.findByIdAndDelete(id)
        .then(() =>{
            console.log(`\x1b[31mBlog ${id} deleted successfully\x1b[89m\x1b[0m\x1b[0m`);
            res.send('OK');
        })
        .catch((err) => console.log(err));
})
app.use((req, res) => { 
    res.status(404).sendFile('./404.html', {root: __dirname})
});

const db = process.env.NODE_ENV == 'production' ? process.env.MONGODB_PRODUCTION : process.env.MONGODB_DEVELOPMENT;
mongoose.connect(db)
    .then((result) => { 
        console.log('DB connected');
        app.listen(3000, () => {
            console.log('Listening to request on port 3000');
        })
    })
    .catch((err) => console.log(err));