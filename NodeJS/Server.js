const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    //console.log(req.url, req.method);

    //res.setHeader('Content-Type', 'text/plain');
    //res.write('Hello World');
    //res.end();
    res.setHeader('Content-Type', 'text/html');
    let path = './';
    switch (req.url) {
        case '/': 
            path += 'Index.html'; 
            break;
        case '/about': 
            path += 'About.html';
            break;
        case '/aboutme': 
            res.statusCode = 301; 
            res.setHeader('Location', '/about'); 
            res.end(); 
            return;
        case '/favicon.ico': 
            path += 'favicon.ico'; 
            res.setHeader('Content-Type', 'image/vnd.microsoft.icon');
            break;
        default: 
            res.statusCode = 404;
            path += '404.html';
    }
    fs.readFile(path, (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            res.write(data);
        }
        res.end();
    })
});

server.listen(3000, 'localhost', () => {
    console.log('Listening to request on port 3000');
});