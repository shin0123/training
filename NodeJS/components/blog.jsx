function Blog(props)
{
    return (
        <div>
            {props.blogs ? props.blogs.map((blog) => 
                <div className='blog'>
                    <div className='editContainer'>
                        <div className='edit'>
                            <img src='/icons8-add-trash-24.png' onClick={() => {
                                fetch(`/blog/${blog._id}`, {method: 'DELETE'})
                                    .then(() => props.deleteBlog(blog._id))
                                    .catch(err => console.log(err));
                                }}></img>
                            <img src='/icons8-edit-24.png' onClick={() => {
                                props.setUpdate(blog._id);
                                }}></img>
                        </div>
                    </div>
                    <div className='date'>{new Date(blog.createdAt).toLocaleString('en-GB')}</div>
                    <div className='title'>{blog.title}</div>
                    <div className='content'>{blog.content}</div>
                </div>) : null}
        </div>
    )
}