function Header(props)
{
    const createBlogs = props.hideCreateBlog ? null : <span className='createBlog' onClick={() => props.setCreateNew()}>Create New Blog</span>;
    return (
        <div className='header'>
            <h1> {props.name} </h1>
            <hr></hr>
            <nav className='nav'>
                <a href="/">Home</a>
                <a href="/about">About</a>
                {createBlogs} 
            </nav>
            <hr></hr>
        </div>
    )
}