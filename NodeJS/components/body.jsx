function Body(props) {
    const [createNew, setCreateNew] = React.useState(false);
    const [blogs, setBlogs] = React.useState([]);
    const [updateBlog, setUpdateBlog] = React.useState();

    function deleteBlog(id)  {
        setBlogs(blogs.filter(x => x._id != id))
    }

    function setUpdate(id) {
        setCreateNew(true);
        setUpdateBlog(blogs.find(x => x._id === id));
    }
    
    React.useEffect(() => {
        fetch('/blog')
            .then(res => res.json())
            .then(data => setBlogs(data))
            .catch(err => console.log(err));
        }, []);
    const form = createNew ? <Form setCreateNew={setCreateNew} blog={updateBlog}/> : null;
    const blog = createNew ? null : (blogs.length > 0 ? <Blog blogs={blogs} deleteBlog={deleteBlog} setUpdate={setUpdate}/> : <div className='message'>No Blogs Created Yet.</div>);
    return (
        <div className='page'>
            <Header name='Welcome To My Blog' setCreateNew={() => setCreateNew(!createNew)} />
            <div className='body'>
                {form}
                {blog}
            </div>
            <Footer/>
        </div>
    )
}