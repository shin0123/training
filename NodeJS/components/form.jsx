function Form(props)
{
    const action = props.blog ? `/blog/${props.blog._id}` : '/blog';
    const title = props.blog ? props.blog.title : null;
    const content = props.blog ? props.blog.content : null;
    return (
        <div>
            <form action={action} method='POST' className='form'>
                <label htmlFor='title'>Blog Title:</label>
                <input id='title' name='title' required defaultValue={title}/>
                <label htmlFor='content'>Blog Content:</label>
                <textarea id='content' name='content' required rows='10' defaultValue={content}/>
                <button type='submit'>Submit</button>
            </form>
        </div>
    )
}