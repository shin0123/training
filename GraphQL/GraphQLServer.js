var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);
 
// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};
 
var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/Index3.html');
});
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');

var sql = require("mssql");

// config for your database
var config = {
    user: 'sa',
    password: 'hc0811!',
    server: 'denis3',
    database: 'abell.81_Dev',
    options: {
        instanceName: 'mssql2017'
    }
};

// connect to your database
sql.connect(config, function (err) {

    if (err) console.log(err);

    // create Request object
    var request = new sql.Request();
        
    // query to the database and get the records
    request.query('select top 1 ObjectID, ObjectName from Customer', function (err, recordset) {
        
        if (err) console.log(err)

        // send records as a response
        console.log(recordset);
    });
});