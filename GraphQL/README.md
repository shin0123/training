# Training materials   
### The folder contains source code for GraphQL training.
* __GraphQLClient.html__: Contains a sample GraphQL client (to be used in future training).
* __GraphQLServer.js__: Contains a sample GraphQL server in Node.JS (to be used in future training).

